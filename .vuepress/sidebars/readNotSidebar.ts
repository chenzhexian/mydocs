export default [
    {
        text: '阅读笔记',
        children: [
            '/docs/readNote/README.md',
            '/docs/readNote/《MySQL是怎样运行的》.md',
        ],
    },
];
