export default [
    {
        text: '常用工具',
        collapsible: true,
        children: [
            '/docs/technicalDocument/常用工具/Stream流式编程.md',
            '/docs/technicalDocument/常用工具/IDEA常用技巧.md',
            '/docs/technicalDocument/常用工具/log日志快速定位.md',
            '/docs/technicalDocument/常用工具/linux常用命令.md',
            '/docs/technicalDocument/常用工具/常用工具网站.md',
        ],
    },
    {
        text: '部署相关',
        collapsible: true,
        children: [
            '/docs/technicalDocument/部署相关/frp内网穿透.md',
            '/docs/technicalDocument/部署相关/minio安装使用.md',
            '/docs/technicalDocument/部署相关/yarn2.0+版本安装.md',
            '/docs/technicalDocument/部署相关/Windows系统后台运行服务.md',
            '/docs/technicalDocument/部署相关/VMware安装Centos7.md',
            '/docs/technicalDocument/部署相关/mysql安装教程.md',
        ],
    },
];
