export default [
    {
        text: '公基',
        collapsible: true,
        children: [
            '/docs/shangan/公基/哲学.md',
            '/docs/shangan/公基/中共党史.md',
            '/docs/shangan/公基/宪法.md',
            '/docs/shangan/公基/刑法.md',
            '/docs/shangan/公基/民法.md',
            '/docs/shangan/公基/行政法.md',
            '/docs/shangan/公基/公务员法.md',
            '/docs/shangan/公基/经济.md',
            '/docs/shangan/公基/公文知识.md',
        ],
    },
    {
        text: '行测',
        collapsible: true,
        children: [
            '/docs/shangan/行测/判断.md',
            {
                text: '资料分析',
                collapsible: true,
                children: [
                    '/docs/shangan/行测/资料分析/资料分析-平均类.md',
                ],
            },
        ],
    },
];
