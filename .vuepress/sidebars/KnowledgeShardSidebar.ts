export default [
    {
        text: '知识碎片',
        children: [
            '/docs/knowledgeShard/README.md',
            '/docs/knowledgeShard/Bean拷贝之MapStruct.md',
            '/docs/knowledgeShard/EasyExcel导出文件.md',
            '/docs/knowledgeShard/LocalDateTime的使用.md',
            '/docs/knowledgeShard/Git 提交竟然还能这么用？.md',
            '/docs/knowledgeShard/后端如何编写单元测试？.md',
            '/docs/knowledgeShard/数据库备份还原.md',
            '/docs/knowledgeShard/mysqlbinlog的使用.md',
            '/docs/knowledgeShard/mybatisplus自定义注入器.md',
            '/docs/knowledgeShard/一篇搞定maven.md',
            '/docs/knowledgeShard/轻松搞定SQL调优.md',
            '/docs/knowledgeShard/设计模式及应用.md',
            '/docs/knowledgeShard/SpringSecurity整合SAS.md',
        ],
    },
];
