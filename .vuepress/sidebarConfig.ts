import technicalDocumentSideBar from "./sidebars/TechnicalDocumentSidebar";
import knowledgeShardSidebar from "./sidebars/KnowledgeShardSidebar";
import readNotSidebar from "./sidebars/readNotSidebar";
import shanganSidebar from "./sidebars/shanganSidebar";
export default {
    '/docs/technicalDocument/': technicalDocumentSideBar,
    '/docs/knowledgeShard/': knowledgeShardSidebar,
    '/docs/readNote/': readNotSidebar,
    '/docs/shangan/': shanganSidebar,
};
