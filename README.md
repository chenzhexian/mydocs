---
home: true
heroText: "小弦🐟の学习笔记"
tagline: ✨每天都进步一点点✨
heroImage: https://czxcab.cn/file/myblog/me.jpg

actions:
- text: 留言板
  link: https://czxcab.cn/message
  type: primary
- text: 工具网站
  link: /docs/technicalDocument/常用工具/常用工具网站.md
  type: secondary
- text: 关于我
  link: https://czxcab.cn/about
  type: secondary
    
footer: 闽ICP备2023019060号-1 | Copyright © 2023-present czx
---

## 本站介绍
### 搭建目的
为什么站主要搭建这个网站呢？理由很简单，站主我记性有点差，所以搭建这个网站记录一下平时开发过程中遇到的各种问题，以及一些常用工具，也希望能帮助到更多的人
### 智能AI助手
站主集成了开源项目[FastGPT](https://github.com/labring/FastGPT)，实现了智能聊天功能，点击右边的蓝色闪电图标就可以开始和AI聊天了 （**请大家别过度使用，token数要收费的 T_T**）
### 部分笔记不全问题
目前部分笔记还在整理中，部分笔记会有缺失问题，可能只上传了整理好的部分
### 最后
笔记大部分内容是方便站主个人查询，博主文档输出能力有限，如果有表述不清楚的地方，敬请谅解。 对笔记有疑问或有其他问题，可以提报给智能AI助手，站主会第一时间回复。
