---
head:
   - - meta
     - name: mysql安装
       content: mysql安装教程
title: mysql安装教程
---

## 一、卸载MySQL
1. 检查是否安装
    ```shell
    rpm -qa | grep mysql
    ```

2. 如已安装mysql，则删除
    ```shell
    rpm -e --nodeps +包名
    ```
3. 再次检查安装包是否全部删除
4. 搜索mysql文件夹
    ```shell
    find / -name mysql
    ```
5. 若有mysql文件夹，则删除
    ```shell
    rm -rf +包名
    ```
6. 再次检查文件夹是否全部删除

## 二、下载MySQL安装包
官网地址：[https://downloads.mysql.com/archives/community/](https://downloads.mysql.com/archives/community/)

1. 选择好版本，点击下载
    ![图片](https://czxcab.cn/file/docs/1718262734732.jpg)
2. 上传到服务器
   ![图片](https://czxcab.cn/file/docs/1718262898072.jpg)
3. 解压
    ```shell
    tar -zxvf mysql-5.7.27-linux-glibc2.12-x86_64.tar.gz
    ```
   ![图片](https://czxcab.cn/file/docs/1718263237207.jpg)
4. 移动文件夹到/usr/local，并重命名为mysql
    ```shell
    mv mysql-5.7.27-linux-glibc2.12-x86_64 /usr/local/mysql
    ```
   ![图片](https://czxcab.cn/file/docs/1718263332203.jpg)
## 三、创建mysql用户组和用户并修改权限
```shell
groupadd mysql 	#创建一个新的用户组，名为“mysql”
useradd -r -g mysql mysql 	#创建一个新的用户，也名为“mysql”
```
## 四、创建目录并赋予权限
```shell
mkdir -p  /data/mysql              #创建目录
chown mysql:mysql -R /data/mysql   #赋予权限
```
## 五、配置my.cnf文件
```shell
vi /etc/my.cnf
```
```text
[mysqld]
datadir=/data/mysql
port=3306
character_set_server=utf8mb4
log-error=/data/mysql/mysql.err
pid-file=/data/mysql/mysql.pid
# 开启 Binlog 并写明存放日志的位置
log_bin = mysql_binlog
# 指定一个集群内的 MySQL 服务器 ID，如果做数据库集群那么必须全局唯一，一般来说不推荐 指定 server_id 等于 1。
server_id = 1
# 设置方面提到过的三种 Binlog 的日志模式
binlog_format = MIXED
```
## 六、初始化数据库
1. 进入bin目录下
   ```shell
   cd /usr/local/mysql/bin/
   ```
2. 初始化
   ```shell
   ./mysqld --defaults-file=/etc/my.cnf --basedir=/usr/local/mysql/ --datadir=/data/mysql/ --user=mysql --initialize
   ```
3. 异常处理
   ![图片](https://czxcab.cn/file/docs/1718269508371.jpg)
   这个错误信息表明，mysqld 进程在尝试加载 libaio.so.1 这个共享库文件时失败了，因为系统找不到这个文件。libaio（异步I/O库）是MySQL在某些操作系统上需要的依赖库之一。 
   
   要解决这个问题，你需要安装 libaio 库。这通常可以通过你的操作系统的包管理器来完成。以下是一些常见Linux发行版的安装命令：
   ```shell
   sudo yum install libaio
   ```
4. 查看密码
   ```shell
   cat /data/mysql/mysql.err
   ```
   ![图片](https://czxcab.cn/file/docs/1718269739701.jpg)
5. 将mysql.server放置到/etc/init.d/mysql中
   ```shell
   cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql
   ```
   > 将 mysql.server 脚本放置到 /etc/init.d/mysql 目录中的主要作用是使得你可以使用标准的系统服务管理工具来管理 MySQL 服务。在 Linux 系统中，/etc/init.d/ 目录存放的是一系列系统服务的管理（启动、停止等）脚本。
   > 
   > mysql.server 是一个官方针对 Unix 和类 Unix 系统二进制版本安装包中包含的脚本，它主要用于启动、查看和停止 mysqld 进程服务。当 mysql.server 脚本被放置在 /etc/init.d/mysql 目录中后，你可以使用 service 命令来管理 MySQL 服务，例如：
   > 
   > service mysql start：启动 MySQL 服务。
   > 
   > service mysql stop：停止 MySQL 服务。
   > 
   > service mysql restart：重启 MySQL 服务。
   > 
   > 这样，你就可以很方便地通过标准的系统服务管理工具来管理 MySQL 服务了。不过，在实际操作中，你还需要确保 MySQL 的配置文件（如 my.cnf）已经正确设置，并且 MySQL 的数据目录和其他相关目录的权限和所有权设置也是正确的。

6. 启动
   ```shell
   #启动
   service mysql start
   #查询
   service mysql status
   ```
7. 设置环境变量
   ```shell
   vi /etc/profile
   ```
   然后最后一行插入：
   ```shell
   export PATH=$PATH:/usr/local/mysql/bin
   ```
   刷新配置：
   ```shell
   source /etc/profile
   ```
   检查环境，确认 MySQL 及其工具是否已经安装在你的系统上
   ```shell
   whereis mysql;
   whereis mysqldump;
   ```
   ![图片](https://czxcab.cn/file/docs/1718270383182.jpg)

## 七、配置MySQL
1. 进入mysql，输入刚刚获取的密码
   ```shell
   mysql -u root -p
   ```
2. 设置密码
   ```mysql
   # 设置
   ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '密码';
   # 刷新
   flush privileges;
   ```
   ![图片](https://czxcab.cn/file/docs/1718270645662.jpg)
3. 配置远程连接
   ```mysql
   use mysql;
   update user set host='%' where user='root';
   flush privileges;
   ```
## 八、开放3306端口
```shell
# 管理防火墙
firewall-cmd --state                   # 查看防火墙状态
systemctl start firewalld              # 开启防火墙
systemctl stop firewalld.service       # 停止防火墙
systemctl disable firewalld.service    # 禁止防火墙开机启动
systemctl restart firewalld.service    # 重启防火墙

# 开放3306端口
firewall-cmd --zone=public --add-port=3306/tcp --permanent
# 开放端口之后 需要重新启动防火墙
systemctl restart firewalld.service    # 重启防火墙 
```
   
