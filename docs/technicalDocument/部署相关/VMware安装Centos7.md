---
head:
  - - meta
    - name: VMware
      content: VMware安装Centos7
    - name: Centos7
title: VMware安装Centos7
---

## 一、准备工作

1.VMware虚拟机（点击查看下载安装教程）

2.centos7镜像下载
（[阿里镜像下载链接](https://mirrors.aliyun.com/centos/7.9.2009/isos/x86_64/)）
（[官网下载链接](https://mirror-hk.koddos.net/centos/7.9.2009/isos/x86_64/)）

![图片](https://czxcab.cn/file/docs/1717469568472.jpg)

## 二、创建虚拟机

1.打开VMware，点击创建新的虚拟机
![图片](https://czxcab.cn/file/docs/1717469730731.jpg)
2.选中自定义，点击下一步

![图片](https://czxcab.cn/file/docs/1717469812237.jpg)
![图片](https://czxcab.cn/file/docs/1717471053102.jpg)
![图片](https://czxcab.cn/file/docs/1717471214352.jpg)
![图片](https://czxcab.cn/file/docs/1717472011141.jpg)
![图片](https://czxcab.cn/file/docs/1717472482182.jpg)
![图片](https://czxcab.cn/file/docs/1717473212383.jpg)
![图片](https://czxcab.cn/file/docs/1717473312393.jpg)
![图片](https://czxcab.cn/file/docs/1717473367364.jpg)

3.然后一直下一步到这个页面，点击自定义硬件
![图片](https://czxcab.cn/file/docs/1717473575363.jpg)
4.配置centos7文件
![图片](https://czxcab.cn/file/docs/1717478447953.jpg)
## 三、安装Centos
1.选第一个
![图片](https://czxcab.cn/file/docs/1717478966594.jpg)
2.选中文
![图片](https://czxcab.cn/file/docs/1717478986263.jpg)
3.软件选择，选上开发工具
![图片](https://czxcab.cn/file/docs/1717479004162.jpg)
![图片](https://czxcab.cn/file/docs/1717479020767.jpg)
4.选安装位置，点我要配置分区，点完成，选标准分区，然后按图加挂载点
![图片](https://czxcab.cn/file/docs/1717479052613.jpg)
![图片](https://czxcab.cn/file/docs/1717479070742.jpg)
![图片](https://czxcab.cn/file/docs/1717479088833.jpg)
![图片](https://czxcab.cn/file/docs/1717479105504.jpg)
![图片](https://czxcab.cn/file/docs/1717479119745.jpg)
![图片](https://czxcab.cn/file/docs/1717479130600.jpg)
![图片](https://czxcab.cn/file/docs/1717479325460.jpg)
5.禁用kdump
![图片](https://czxcab.cn/file/docs/1717479338435.jpg)
![图片](https://czxcab.cn/file/docs/1717479350170.jpg)
6.开启网络
![图片](https://czxcab.cn/file/docs/1717479366581.jpg)
![图片](https://czxcab.cn/file/docs/1717479379142.jpg)
